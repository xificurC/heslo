#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define BASE_LEN 128
#define ROOT_LEN 7 // /.heslo is 7 bytes

char BASE_DIR[BASE_LEN];
int BASE_DIR_LEN = -1;
const char *ROOT = "/.heslo";


/* First check for env var HESLO_BASE_DIR
 * otherwise use default ~/.heslo.
 * Sets up BASE_DIR.
 * Returns length of BASE_DIR or -1 if failure.
 */
static void
base_dir()
{
        char *envvar;
        size_t len;
        if ((envvar = getenv("HESLO_BASE_DIR"))) {
                len = strlen(envvar);
                if (len >= BASE_LEN) {
                        fprintf(stderr, "base directory read from HESLO_BASE_DIR "
                                "'%s' longer than maximum supported length %d\n",
                                envvar, BASE_LEN);
                        exit(1);
                }
                strcpy(BASE_DIR, envvar);
                if (BASE_DIR[len-1] == '/') {
                        BASE_DIR[len-1] = '\0';
                        len--;
                }
                BASE_DIR_LEN = len;
                exit(1);
        }
        if (!(envvar = getenv("HOME"))) {
                fprintf(stderr, "no HOME or HESLO_BASE_DIR set, "
                        "can't guess password store location.\n");
                exit(1);
        }
        len = strlen(envvar);
        if (len + ROOT_LEN >= BASE_LEN) {
                fprintf(stderr, "'%s%s' longer than maximum supported length %d\n",
                        envvar, ROOT, BASE_LEN);
                exit(1);
        }
        char *at = BASE_DIR;
        strncpy(at, envvar, len);
        strcpy(at + len, ROOT);
        BASE_DIR_LEN = len + ROOT_LEN;
}

static void
usage()
{
        printf("heslo [get|set|init] ARG...\n");
}

static void
rot128(char *str, size_t count)
{
        while (count--)
                *(str++) += 128;
}

static void
decode(char *str, size_t count)
{
        rot128(str, count);
}

static void
encode(char *str, size_t count)
{
        rot128(str, count);
}

static void
fullpath(char *filename, char *ref)
{
        strncpy(ref, BASE_DIR, BASE_DIR_LEN);
        ref += BASE_DIR_LEN;
        *(ref++) = '/';
        strcpy(ref, filename);
}

static void
get(char *filename)
{
        const size_t BUFFER_SIZE = 1024;
        size_t count;
        int fd;
        char buffer[BUFFER_SIZE];
        char file[BASE_LEN + 1 + strlen(filename)]; // base + / + file
        fullpath(filename, file);
        if ((fd = open(file, O_RDONLY, 0)) < 0) {
                perror(NULL);
                fprintf(stderr, "get error for filename '%s'\n", file);
                exit(1);
        }
        while ((count = read(fd, buffer, BUFFER_SIZE)) > 0) {
                decode(buffer, count);
                write(1, buffer, count);
        }
        close(fd);
}

static void
make_dir(char *dir)
{
        if (mkdir(dir, 0700) < 0 && errno != EEXIST) {
                perror(NULL);
                fprintf(stderr, "failed to create directory '%s'\n", dir);
                exit(1);
        }
}

static void
mkdirs(char *dir)
{
        size_t len = strlen(dir);
        char _dir[len+1];
        strcpy(_dir, dir);
        char *at = _dir;
        while (*(++at) != '\0') {
                if (*at == '/') {
                        *at = '\0';
                        make_dir(_dir);
                        *at = '/';
                }
        }
        make_dir(_dir);
}

static void
ensure_file_directory(char *filename)
{
        char *dirend = filename + strlen(filename);
        while (*(--dirend) != '/');
        *dirend = '\0';
        mkdirs(filename);
        *dirend = '/';
}

static size_t
read_stdin(char *content, size_t max_len)
{
        ssize_t read_bytes = read(0, content, max_len);
        if (read_bytes < 0) {
                perror(NULL);
                fprintf(stderr, "failed to read stdin\n");
                exit(1);
        }
        if ((size_t) read_bytes >= max_len) {
                fprintf(stderr, "content longer than allowed maximum %lu\n", max_len);
                exit(1);
        }
        content[read_bytes] = '\0';
        return read_bytes;
}

static void
cleanup_file(char *file)
{
        if ((unlink(file)) < 0) {
                perror(NULL);
                fprintf(stderr, "failed to clean up file '%s'\n", file);
        }
}

static void
set(char *filename)
{
        size_t length = 1024;
        char content[length];
        int fd;
        length = read_stdin(content, length - 1);
        encode(content, length);
        char file[BASE_LEN + 1 + strlen(filename)]; // base + / + file
        fullpath(filename, file);
        ensure_file_directory(file);
        if ((fd = open(file, O_WRONLY|O_CREAT|O_TRUNC, 0600)) < 0) {
                perror(NULL);
                fprintf(stderr, "set error for filename '%s'\n", file);
                exit(1);
        }
        ssize_t write_result;
        if ((write_result = write(fd, content, length)) < 0) {
                perror(NULL);
                fprintf(stderr, "failed to write into '%s'\n", file);
                close(fd);
                cleanup_file(file);
                exit(1);
        } else if ((size_t) write_result < length) {
                fprintf(stderr, "failed to write all data to '%s'\n", file);
                close(fd);
                cleanup_file(file);
                exit(1);
        }
        close(fd);
}

int
main(int argc, char *argv[])
{
        if (argc < 2) {
                usage();
                exit(1);
        }
        char *command = argv[1];
        switch (command[0]) {
        case 'g':
                base_dir();
                if (argc < 3) {
                        usage();
                        exit(1);
                }
                get(argv[2]);
                break;
        case 's':
                base_dir();
                if (argc < 3) {
                        usage();
                        exit(1);
                }
                set(argv[2]);
                break;
        case 'i':
                printf("init\n");
                break;
        default:
                usage();
                exit(1);
        }
}
