# See LICENSE file for copyright and license details.
include config.mk

SRC = heslo.c
OBJ = ${SRC:.c=.o}

all: heslo

options:
	@echo heslo build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.o:
	$(LD) -o $@ $< $(LDFLAGS)

.c.o:
	$(CC) -c -o $@ $< $(CFLAGS)

heslo: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

# ${OBJ}: arg.h

install: all
	mkdir -p ${DESTDIR}${DOCDIR}
	mkdir -p ${DESTDIR}${BINDIR}
	mkdir -p ${DESTDIR}${MAN1DIR}
	install -d ${DESTDIR}${BINDIR} ${DESTDIR}${MAN1DIR}
	install -m 644 CHANGES README FAQ LICENSE ${DESTDIR}${DOCDIR}
	install -m 775 heslo ${DESTDIR}${BINDIR}
	sed "s/VERSION/${VERSION}/g" < heslo.1 > ${DESTDIR}${MAN1DIR}/heslo.1
	chmod 644 ${DESTDIR}${MAN1DIR}/heslo.1

uninstall: all
	rm -f ${DESTDIR}${MAN1DIR}/heslo.1 \
		${DESTDIR}${BINDIR}/heslo
	rm -rf ${DESTDIR}${DOCDIR}

dist: clean
	mkdir -p heslo-${VERSION}
	cp -R Makefile CHANGES README FAQ LICENSE strlcpy.c arg.h \
		config.mk heslo.c heslo.1 heslo-${VERSION}
	tar -cf heslo-${VERSION}.tar heslo-${VERSION}
	gzip heslo-${VERSION}.tar
	rm -rf heslo-${VERSION}

clean:
	rm -f heslo *.o
